import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

// General modules
import bubbles from './modules/bubbles';
import rsidebar from './modules/rsidebar';
import down from './modules/down';
import modal from './modules/modal';
import notmodal from './modules/notmodal';
import extensions from './modules/extensions';
import events from './modules/events';


// Extension modules
import extension_deadline from './extensions/extension_deadline';

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
	modules: {
		bubbles,
		rsidebar,
		down,
		modal,
		notmodal,
		extensions,
		events,

		extension_deadline,
        // FullCalendar
	}
});
