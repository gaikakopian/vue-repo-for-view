
//---------//
// Imports //
//---------//
// import user from './user'


//-------//
// State //
//-------//
const state = {
	datetime: ""
};


//-----------//
// Mutations //
//-----------//
const mutations = {

	/**
 	* Collapse right sidebar if it's expanded, expand if it's collapsed.
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	setDatetime(state, data) {

		state.datetime = moment(data.deadline_at).format('YYYY-MM-DDTHH:mm:ss');

	},

};


//---------//
// Actions //
//---------//
const actions = {

};


//---------//
// Getters //
//---------//
const getters = {

};


//---------//
// Modules //
//---------//
const modules = {

};


//--------//
// Export //
//--------//
export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}