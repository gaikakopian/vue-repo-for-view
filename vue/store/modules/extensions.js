
//---------//
// Imports //
//---------//
// import user from './user'


//-------//
// State //
//-------//
const state = {
	list: [],
	in_progress: false
};


//-----------//
// Mutations //
//-----------//
const mutations = {

	/**
 	* Update in_progress value
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	in_progress(state, data) {
		state.in_progress = data;
	},

	/**
	 * Update extensions by data
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	list(state, data) {
		state.list = data;
	},

};


//---------//
// Actions //
//---------//
const actions = {

};


//---------//
// Getters //
//---------//
const getters = {
	getChosen(state, getters) {
		return state.chosen;
	}
};


//---------//
// Modules //
//---------//
const modules = {

};


//--------//
// Export //
//--------//
export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}