
//---------//
// Imports //
//---------//
// import user from './user'


//-------//
// State //
//-------//
const state = {
	active: false,
	title: "Notmodal",
	modals: {
		list: [
			"add",
			"settings",
			"event_chosen"
		],
		chosen: "add",
		add: {
			list: [
				"choose",
				"deadline",
				"support",
			],
			chosen: "choose"
		},
		settings: {
			list: [
				"deadline",
				"support",
			],
			chosen: "",
			instance: ""
		},
		event_chosen: {
			chosen: "",
			instance: ""
		},
	},
	meta: {}
};


//-----------//
// Mutations //
//-----------//
const mutations = {

	/**
 	* Show the notmodal window
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	show(state, data) {
		state.active = true;
	},

	/**
 	* Hide the notmodal window
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	hide(state, data) {
		state.active = false;
	},

	/**
 	* Switch the notmodal window
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	_switch(state, data) {
		state.active = !state.active;
	},

	/**
 	* Choose modal to display
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	choose(state, data) {

		// Choose modal
		if(state.modals.list.indexOf(data.modal) === -1)
			state.modals.chosen = state.modals.list[0];
		else
			state.modals.chosen = state.modals.list[state.modals.list.indexOf(data.modal)];

		// Set meta
		state.meta = data.meta;

	},

	/**
 	* Choose add modal to display
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	choose_add(state, data) {

		// Choose add modal
		if(state.modals.add.list.indexOf(data.modal) === -1)
			state.modals.add.chosen = state.modals.add.list[0];
		else
			state.modals.add.chosen = state.modals.add.list[state.modals.add.list.indexOf(data.modal)];

	},

	/**
 	* Choose settings modal to display
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	choose_settings(state, data) {

		// Choose settings modal
		if(state.modals.settings.list.indexOf(data.modal) === -1)
			state.modals.settings.chosen = state.modals.settings.list[0];
		else
			state.modals.settings.chosen = state.modals.settings.list[state.modals.settings.list.indexOf(data.modal)];

		// Choose instance
		state.modals.settings.instance = data.instance;

	},
	

};


//---------//
// Actions //
//---------//
const actions = {

};


//---------//
// Getters //
//---------//
const getters = {
	getChosen(state, getters) {
		return state.chosen;
	}
};


//---------//
// Modules //
//---------//
const modules = {

};


//--------//
// Export //
//--------//
export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}