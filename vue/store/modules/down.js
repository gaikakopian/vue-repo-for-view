
//---------//
// Imports //
//---------//
// import user from './user'


//-------//
// State //
//-------//
const state = {
	active: false
};


//-----------//
// Mutations //
//-----------//
const mutations = {

	/**
 	* Collapse right sidebar if it's expanded, expand if it's collapsed.
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	setActive(state, data) {

		let max = data.max;
		let current = data.current;
		let delta = max - current;

		if(max < 0) state.active = false;
		else state.active = delta > 100;

	},

	width(state, data) {

		// set width
		state.width = state.expanded ? styles.rsidebarExpandedWidth : styles.rsidebarWidth;

	}

};


//---------//
// Actions //
//---------//
const actions = {

};


//---------//
// Getters //
//---------//
const getters = {
	getChosen(state, getters) {
		return state.chosen;
	}
};


//---------//
// Modules //
//---------//
const modules = {

};


//--------//
// Export //
//--------//
export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}