
//---------//
// Imports //
//---------//
// import user from './user'
import styles from '../../../../shared/scss/_variables.scss'


//-------//
// State //
//-------//
const state = {
	expanded: (function(){
		if(server.data.rsidebar_switcher_state !== true && server.data.rsidebar_switcher_state !== false)
			return true;
		else
			return server.data.rsidebar_switcher_state;
	})(),
	width: (function(){
		if(server.data.rsidebar_switcher_state !== true && server.data.rsidebar_switcher_state !== false)
			return parseInt(styles.rsidebarExpandedWidth);
		else
			return server.data.rsidebar_switcher_state ? parseInt(styles.rsidebarExpandedWidth) : parseInt(styles.rsidebarWidth);
	})(),
	rmenu: {
		list: [
            {
                type: 'item',
                title: 'info',
                icon_mdi: 'info'
            },
			{
				type: 'item',
				title: 'members',
				icon_mdi: 'members'
			},
            {
                type: 'item',
                title: 'events',
                icon_mdi: 'calendar'
            },
			{
				type: 'item',
				title: 'files',
				icon_mdi: 'file'
			},
            {
                type: 'item',
                title: 'settings',
                icon_mdi: 'settings'
            }
		],
		chosen: ""
	}
};


//-----------//
// Mutations //
//-----------//
const mutations = {

	/**
 	* Collapse right sidebar if it's expanded, expand if it's collapsed.
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	switch(state, data) {

		// switch right panel
		state.expanded = !state.expanded;

		// update user right panel switcher cookie
		axios.post('/api/core/other/switcher', {
			expanded: state.expanded
		})
			.then(function (response) {
				//toastr.info('Позиция правого сайдбара успешно сохранена')
			})
			.catch(function (error) {
				console.log(error);
			});

	},

	/**
 	* Expand right sidebar if it's not expanded
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	expand(state, data) {

		// switch right panel, if it is not expanded
		if(!state.expanded)
			state.expanded = !state.expanded;
		else
			return;

		// update user right panel switcher cookie
		axios.post('/api/core/other/switcher', {
			expanded: state.expanded
		})
			.then(function (response) {
				//toastr.info('Позиция правого сайдбара успешно сохранена')
			})
			.catch(function (error) {
				console.log(error);
			});

	},

	/**
 	* Collapse right sidebar if it's not expanded
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	collapse(state, data) {

		// switch right panel, if it is expanded
		if(state.expanded)
			state.expanded = !state.expanded;
		else
			return;

		// remove chosen
		state.rmenu.chosen = "";

		// update user right panel switcher cookie
		axios.post('/api/core/other/switcher', {
			expanded: state.expanded
		})
			.then(function (response) {
				//toastr.info('Позиция правого сайдбара успешно сохранена')
			})
			.catch(function (error) {
				console.log(error);
			});

	},

	/**
 	* Calculate width
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	width(state, data) {

		// set width
		state.width = state.expanded ? styles.rsidebarExpandedWidth : styles.rsidebarWidth;

	},

	/**
 	* Choose menu item
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	rmenu_choose(state, data) {

		state.rmenu.chosen = data;

	}

};


//---------//
// Actions //
//---------//
const actions = {

};


//---------//
// Getters //
//---------//
const getters = {
	getChosen(state, getters) {
		return state.chosen;
	}
};


//---------//
// Modules //
//---------//
const modules = {

};


//--------//
// Export //
//--------//
export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}