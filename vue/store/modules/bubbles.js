
//---------//
// Imports //
//---------//
// import user from './user'


//-------//
// State //
//-------//
const state = {
	bubbles: '',
	chosen: '',
	websocket: {
		files: [],
		messages: []
	}
};


//-----------//
// Mutations //
//-----------//
const mutations = {

	/**
	 * Choose clicked bubble
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	choose(state, data) {
		state.chosen = data;
	},

	/**
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	chooseById(state, data) {
		for(let i=0; i<state.bubbles.length; i++) {
			if(data.id === state.bubbles[i].id)
				state.chosen = state.bubbles[i];
		}
	},

	/**
	 * Update bubbles by data
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	bubbles(state, data) {
		state.bubbles = data;
	},

	/**
	 * Push message to the currently chosen bubble
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	pushMessage(state, data) {

		// Find bubble to which needs to add a new message
		let bubble = (function(){
			for(let i=0; i<state.bubbles.length; i++) {
				if(+state.bubbles[i].id === +data['id_bubble'])
					return state.bubbles[i];
			}
		})();

		// Add a new message to bubble, if it's not null
		if(bubble)
			bubble.messages.push(data.message);

	},

	/**
	 * Push new file data to the currently chosen bubble
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	pushNewFile(state, data) {

		// Find bubble to which needs to add a new file
		let bubble = (function(){
			for(let i=0; i<state.bubbles.length; i++) {
				if(+state.bubbles[i].id === +data['id_bubble'])
					return state.bubbles[i];
			}
		})();

		// Add a new file to bubble, if it's not null
		if(bubble)
			bubble.files.push(data.file);

	},

	/**
	 * Push a member to the currently chosen bubble
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	pushMember(state, data) {

		// Add a new member to bubble, if it's not null
		state.chosen.members.push(data.member);

	},

	/**
	 * Remove a member to the currently chosen bubble
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	removeMember(state, data) {

		// Remove a member from bubble
		state.chosen.members = state.chosen.members.filter((item) => {
			return item.id != data.member.id;
		});

	},

	/**
	 * Add a value to the ws index
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	addToWSIndex(state, data) {

		// Add a new member to bubble, if it's not null
		state.websocket[data.index].push(data.id_bubble);

	},

	/**
	 * Push an extension to the currently chosen bubble
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	pushExtension(state, data) {

		// Add a new extension to bubble, if it's not null
		state.chosen.extensions.push(data.extension);

	},

	/**
	 * Remove an extension from the currently chosen bubble
	 * @param state Store state link
	 * @param data Passed to the mutator data
	 */
	removeExtension(state, data) {

		// Remove an extension from bubble
		state.chosen.extensions = state.chosen.extensions.filter((item) => {
			return item.id != data.extension_id;
		});

	},

};


//---------//
// Actions //
//---------//
const actions = {

};


//---------//
// Getters //
//---------//
const getters = {
	getChosen(state, getters) {
		return state.chosen;
	}
};


//---------//
// Modules //
//---------//
const modules = {

};


//--------//
// Export //
//--------//
export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}