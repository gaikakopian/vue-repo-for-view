
//---------//
// Imports //
//---------//
// import user from './user'


//-------//
// State //
//-------//
const state = {
    events: '',
    chosen: '',
    websocket: {
        files: [],
        messages: []
    }
};


//-----------//
// Mutations //
//-----------//
const mutations = {

    /**
     * Choose clicked bubble
     * @param state Store state link
     * @param data Passed to the mutator data
     */
    choose(state, data) {
        state.chosen = data;
    },

};


//---------//
// Actions //
//---------//
const actions = {

};


//---------//
// Getters //
//---------//
const getters = {
    getChosen(state, getters) {
        return state.chosen;
    }
};


//---------//
// Modules //
//---------//
const modules = {

};


//--------//
// Export //
//--------//
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}