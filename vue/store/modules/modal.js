
//---------//
// Imports //
//---------//
// import user from './user'


//-------//
// State //
//-------//
const state = {
	active: false,
	title: "Modal",
	modals: {
		list: [
			"add_member"
		],
		chosen: ""
	},
	meta: {}
};


//-----------//
// Mutations //
//-----------//
const mutations = {

	/**
 	* Show the modal window
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	show(state, data) {
		state.active = true;
	},

	/**
 	* Hide the modal window
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	hide(state, data) {
		state.active = false;
	},

	/**
 	* Choose modal to display
	* @param state Store state link
	* @param data Passed to the mutator data
 	*/
	choose(state, data) {

		// Choose modal
		if(state.modals.list.indexOf(data.modal) === -1)
			state.modals.chosen = state.modals.list[0];
		else
			state.modals.chosen = state.modals.list[state.modals.list.indexOf(data.modal)];

		// Set meta
		state.meta = data.meta;

	}


};


//---------//
// Actions //
//---------//
const actions = {

};


//---------//
// Getters //
//---------//
const getters = {
	getChosen(state, getters) {
		return state.chosen;
	}
};


//---------//
// Modules //
//---------//
const modules = {

};


//--------//
// Export //
//--------//
export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}