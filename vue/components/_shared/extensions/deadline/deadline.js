export default {
	name: 'Deadline',
	props: {
		usecase: {
			default: "add"   // add, settings, widget
		},
		instance: {},
		index: 0
	},
	data () {
		return {
			store: "",
			datetime4add: ""
		}
	},
	methods: {
		hide: function(data){
			this.store.commit('notmodal/hide', {})
		},
		add: function(){

			// 1. If date is empty, return
			if(!this.datetime4add) return;

			// 2. Post a new message
			axios.post('/api/extensions/deadline/add', {
				bubble_id: this.store.state.bubbles.chosen.id,
				deadline_at: this.datetime4add
			})
				.then((response) => {

					// Notify
					toastr.success('A new deadline extension instance is successfully added to the bubble.');

					// Add a new instance to the current bubble
					this.store.commit('bubbles/pushExtension', { extension: response.data });

					// Hide
					this.store.commit('notmodal/hide', {});

				})
				.catch(function (error) {
					toastr.error('<h2>Error #'+error.response.status+' </h2><br>'+error.response.data.message);
					console.log(error);
				});

		},
		update: function(){

			// 1. If date is empty, return
			if(!this.datetime) return;

			// 2. Do nothing if new and old values are equal
			if(moment(this.datetime).utc().valueOf() === moment(this.instance.data.deadline_at).utc().valueOf())
				return;

			// 3. Post a new message
			axios.post('/api/extensions/deadline/update', {
				instance_id: this.instance.id,
				deadline_at: this.datetime
			})
				.then((response) => {

					// Notify
					toastr.success('The deadline extension has been successfully updated.');

					// Hide
					this.store.commit('notmodal/hide', {});

				})
				.catch(function (error) {
					toastr.error('<h2>Error #'+error.response.status+' </h2><br>'+error.response.data.message);
					console.log(error);
				});

		},
		deleteInstance: function(){

			// 1. Do you really want to delete?
			let really = confirm("Do you really wanna delete the extension instance?");
			if(!really)
				return;

			// 2. Post a new message
			axios.post('/api/extensions/deadline/delete', {
				instance_id: this.instance.id
			})
				.then((response) => {

					// Notify
					toastr.success('The deadline extension has been successfully deleted.');

					// Remove an instance from the current bubble
					this.store.commit('bubbles/removeExtension', { extension_id: this.instance.id });

					// Hide
					this.store.commit('notmodal/hide', {});

				})
				.catch(function (error) {
					toastr.error('<h2>Error #'+error.response.status+' </h2><br>'+error.response.data.message);
					console.log(error);
				});

		},
		openSettingsNotmodal: function(){

			// 1. Open notmodal window with extension settings
			this.store.commit('notmodal/show', {});
			this.store.commit('notmodal/choose', { modal: 'settings' });
			this.store.commit('notmodal/choose_settings', { modal: 'deadline', instance: this.instance });

		},
	},
	created: function(e) {

		// 1. Set store
		this.store = this.$store.default;

		// 2. If usecase is 'widget' or 'settings'
		if(['widget'].indexOf(this.usecase) !== -1) {

			// 1] Set deadline start value
			this.datetime = this.instance.data.deadline_at;

		}

	},
	components: {
		'back1': require('$app/_shared/extensions/deadline/back1/back1.vue')
	},
	computed: {
		datetime: {

			get: function () {

				return this.store.state.extension_deadline.datetime;

			},

			set: function (deadline_at) {

				this.store.commit('extension_deadline/setDatetime', { deadline_at: deadline_at });

			}

		}
	}
}