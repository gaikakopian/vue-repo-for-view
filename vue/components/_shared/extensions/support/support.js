export default {
	name: 'Support',
	props: {
		usecase: {
			default: "add"   // add, settings, widget
		},
		instance: {},
		index: 0
	},
	data () {
		return {
			store: "",
			datetime4add: ""
		}
	},
	methods: {
		hide: function(data){
			this.store.commit('notmodal/hide', {})
		},
		deleteInstance: function(){

			// 1. Do you really want to delete?
			let really = confirm("Do you really wanna delete the extension instance?");
			if(!really)
				return;

			// 2. Post a new message
			axios.post('/api/extensions/support/delete', {
				instance_id: this.instance.id
			})
				.then((response) => {

					// Notify
					toastr.success('The support extension has been successfully deleted.');

					// Remove an instance from the current bubble
					this.store.commit('bubbles/removeExtension', { extension_id: this.instance.id });

					// Hide
					this.store.commit('notmodal/hide', {});

				})
				.catch(function (error) {
					toastr.error('<h2>Error #'+error.response.status+' </h2><br>'+error.response.data.message);
					console.log(error);
				});

		},
		openSettingsNotmodal: function(){

			// 1. Open notmodal window with extension settings
			this.store.commit('notmodal/show', {});
			this.store.commit('notmodal/choose', { modal: 'settings' });
			this.store.commit('notmodal/choose_settings', { modal: 'support', instance: this.instance });

		},
	},
	created: function(e) {

		// 1. Set store
		this.store = this.$store.default;

	},
	components: {
		'back1': require('$app/_shared/extensions/support/back1/back1.vue')
	}
}