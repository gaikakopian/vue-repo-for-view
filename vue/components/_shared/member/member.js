export default {
	name: 'member',
	props: {
		usecase: '',
		member: ''
	},
	data () {
		return {
			server: server.data,
			store: ""
		}
	},
	components: {},
	methods: {
		member_add: function(data) {
			this.$emit('member_add', this.member);
		},
		expel_member: function(data) {

			// 1. You can't expell yourself
			if(this.member.id == this.store.state.bubbles.chosen.creator_id) {
				toastr.error("You can't expel a bubble creator!");
				return;
			}

			// 2. Notify about request start
			toastr.info("Please, wait.", "Expelling members...", {timeOut: 99999999});

			// 3. Send a new request
			axios.post('/api/core/bubbles/expel', {
				bubble_id: this.store.state.bubbles.chosen.id,
				chosen: [this.member]
			})
				.then((response) => {

					// 1. Close all toasts
					toastr.clear();

					// 2. Notify about success
					toastr.success("The members have been successfully expelled!");

					// 3. Remove expelled members from the bubble's members
					for(let i = 0; i<response.data.length; i++) {
						this.store.commit('bubbles/removeMember', {
							'member': response.data[i]
						});
					}

				})
				.catch((error) => {
					toastr.error('<h2>Error #'+error.response.status+' </h2><br>'+error.response.data.message);
					console.log(error);
				});

		}
	},
	created: function () {

		// 1] Set store
		this.store = this.$store.default;

	}
}