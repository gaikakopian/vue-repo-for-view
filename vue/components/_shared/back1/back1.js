export default {
	name: 'Back1',
	props: {
		target: String
	},
	data () {
		return {
			store: "",
			active: true
		}
	},
	methods: {
		back: function(){
			this.store.commit('notmodal/choose_add', {
				modal: this.target
			})
		}
	},
	components: {},
	created: function () {

		// 1. Set store
		this.store = this.$store.default;

	}
}