import { Drag, Drop } from 'vue-drag-drop';
export default {

	name: 'Bubble',
	props: {
		data: Object,
		index: Number,
		chosen: [Object, String],
        usecase: String 	// stack, rsidebar
	},
	data () {
		return {
			store: ""
		}
	},
	methods: {
		choose: function(e){

			// emit choose event
			this.$emit('choose', this.data);

		}
	},
	created: function(e) {

		// 1. Set store
		this.store = this.$store.default;

		// 2. Listen to a new messages from the websocket server
		if(this.store.state.bubbles.websocket.messages.indexOf(this.data.id) === -1) {

			// Subscribe
			Echo.private('App.Messages.Bubble_' + this.data.id)
				.listen('BroadcastChatMessage', (e) => {
					this.store.commit('bubbles/pushMessage', {
						'id_bubble': e.id_bubble,
						'message': e.message
					});
				});

			// Add id to the index
			this.store.commit('bubbles/addToWSIndex', {
				'index': 'messages',
				'id_bubble': this.data.id,
			});

		}

		// 3. Listen to a new file messages from the websocket server
		if(this.store.state.bubbles.websocket.files.indexOf(this.data.id) === -1) {

			// Subscribe
			Echo.private('App.Bubble.Files.' + this.data.id)
				.listen('BroadcastBubbleFilesList', (e) => {
					this.store.commit('bubbles/pushNewFile', {
						'id_bubble': e.id_bubble,
						'file': e.file
					});
				});

			// Add id to the index
			this.store.commit('bubbles/addToWSIndex', {
				'index': 'files',
				'id_bubble': this.data.id,
			});

		}

	},
	components: {Drag, Drop}
}