export default {
	name: 'Down',
	props: {
		data: Object,
		index: Number,
		chosen: [Object, String]
	},
	data () {
		return {
			active: false
		}
	},
	methods: {
		down: function(e){
			window.scrollTo(window.scrollX, document.body.scrollHeight);
		}
	},
	components: {},
	created: function () {

		// 1. Set store
		this.store = this.$store.default;

		// 2. Set scroll event handler
		$(document).scroll(() => {

			this.store.commit('down/setActive', {
				max: window.scrollMaxY || (document.documentElement.scrollHeight - document.documentElement.clientHeight),
				current: Math.floor($(document).scrollTop())
			});

		});

	}
}