export default {
	name: 'newbubble',
	props: {
		usecase: String 	// bubble, subbubble
	},
	data () {
		return {
			store: "",
			server: server,
			edit: {
				is_on: false,
				title: ""
			},
		}
	},
	components: {},
	methods: {
		switch_edit: function(data){

			// switch
			this.edit.is_on = !this.edit.is_on;

			// If is_on == true, attach focus to the input element
			if(this.edit.is_on == true) {
				setTimeout(() => {
					this.$refs.nb_input_focus.$el.focus();
				});
			}

			// If is_on == false, clear edit.title
			if(this.edit.is_on == false)
				this.edit.title = "";

		},
		create: function(data){

			// 1. If there is no title, return
			if(!this.edit.title) return;

			// 2. Notify about request start
			toastr.info("Please, wait.", "Creating a new bubble", {timeOut: 99999999});

			// 3. Prepare request parameters object
			let params = {
				title: 				this.edit.title,
				description: 	""
			};
			if(this.usecase === 'subbubble')
				params.parent_id = this.store.state.bubbles.chosen.id;

			// 4. Send a new request
			axios.post('/api/core/bubbles/create', params) //  '/api/bubbles/create/'+this.store.state.bubbles.chosen.id, {
				.then((response) => {

					// 1] Close all toasts
					toastr.clear();

					// 2] Notify about request success
					toastr.success("The new bubble has been created successfully.", "Creating a new bubble");

					// 3] Switch the edit to close state
					this.edit.title = "";
					this.edit.is_on = false;

					// 4] Update the bubbles list
					axios.post('/api/core/bubbles/get', { //  '/api/bubbles/getFilteredAndSortedBubbles', {
						search: ""
					})
						.then((resp) => {

							// 4.1] Update the bubbles list
							this.store.commit('bubbles/bubbles', resp.data);

							// 4.2] Choose bubble
							if(this.usecase === 'subbubble') {

								// bubbles.chosen.id (if usecase is subbubble)
								this.store.commit('bubbles/chooseById', this.store.state.bubbles.chosen);

							} else {

								// response.data bubble (if usecase is bubble)
								this.store.commit('bubbles/chooseById', response.data);

							}

						})
						.catch(function (error) {
						  toastr.error('<h2>Error #'+error.response.status+' </h2><br>'+error.response.data.message);
							console.log(error);
						});//

					// 5] Subscribe to the new bubble messages channel
					//Echo.private('App.Messages.Bubble_'+response.data.id)
					//	.listen('BroadcastChatMessage', (e) => {
					//		this.store.commit('bubbles/pushMessage', {
					//			'id_bubble': e.id_bubble,
					//			'message': e.message
					//		});
					//	});

					// 6] Subscribe to the new bubble files messages channel
					//Echo.private('App.Bubble.Files.'+response.data.id)
					//	.listen('BroadcastBubbleFilesList', (e) => {
					//		this.store.commit('bubbles/pushNewFile', {
					//			'id_bubble': e.id_bubble,
					//			'file': 		 e.file
					//		});
					//	});

					// 7] Scroll bubbles stack down (if usecase is bubble)
					if(this.usecase === 'bubble') {
						setTimeout(function () {
							let el = document.querySelector('.chat .bubbles .blist');
							el.scrollTo(0, el.scrollHeight);
						}, 2000);
					}

					// 8] Scroll subbubbles stack down, if usecase = 'subbubble'
					//if(this.usecase === 'subbubble') {
					//	setTimeout(function () {
					//		let el = document.querySelector('.bubbles .subbubbles');
					//		el.scrollTo(0, el.scrollHeight);
					//	}, 1000);
					//}

				})
				.catch((error) => {
					toastr.error('<h2>Error #'+error.response.status+' </h2><br>'+error.response.data.message);
					console.log(error);
					toastr.clear();
				});

		}
	},
	created: function () {

		// 1] Set store
		this.store = this.$store.default;

		// 2] Set edit.is_on = false when input blur
		setTimeout(() => {
			$(this.$refs.nb_input_focus.$el).blur(this, function(event){
				event.data.edit.is_on = false;
				event.data.edit.title = "";
			});
		});

	}
}