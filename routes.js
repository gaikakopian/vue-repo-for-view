
/**
 * Import store
 */
import store from './vue/store/index'

/**
 * Import necessary components
 */
import chat from './vue/components/contents/chat/chat.vue'
import calendar from './vue/components/contents/calendar/calendar.vue'
import contacts from './vue/components/contents/contacts/contacts.vue'
import settings from './vue/components/contents/settings/settings.vue'
import tree from './vue/components/contents/tree/tree.vue'

/**
 * Export routes
 */
export const routes = [
	{
		path: '/app',
		component: chat,
		beforeEnter: function(to, from, next) {
			next();
		}
	},
	{
		path: '/app/calendar',
		component: calendar,
		beforeEnter: function(to, from, next) {
			next();
		}
	},
	{
		path: '/app/contacts',
		component: contacts,
		beforeEnter: function(to, from, next) {
			next();
		}
	},
	{
		path: '/app/settings',
		component: settings,
		beforeEnter: function(to, from, next) {
			next();
		}
	},
	{
		path: '/app/tree',
		component: tree,
		beforeEnter: function(to, from, next) {
			next();
		}
	}
];




