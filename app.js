

/**
 * Load dependencies
 */
require('../shared/js/bootstrap.js');
window.Vue = require('vue');
import VueMaterial from 'vue-material';
import VueRouter from 'vue-router';
import { routes } from './routes';
import Datetime from 'vue-datetime';
import { Settings } from 'luxon';

/**
 * Add components
 */
Vue.component('lsidebar', require('$app/lsidebar/lsidebar.vue'));
Vue.component('rsidebar', require('$app/rsidebar/rsidebar.vue'));
Vue.component('contents', require('$app/contents/contents.vue'));
Vue.component('modal', require('$app/modal/modal.vue'));
Vue.component('notmodal', require('$app/notmodal/notmodal.vue'));

import { MdField } from 'vue-material/dist/components';
import { MdProgress } from 'vue-material/dist/components';
import { MdDialog } from 'vue-material/dist/components';
import { MdButton } from 'vue-material/dist/components';
import { MdChips } from 'vue-material/dist/components';
import {FullCalendar} from '$app/contents/calendar/vue-full-calendar/index';
import { Drag, Drop } from 'vue-drag-drop';

/**
 * Add plugins
 */
// Vue.use(VueMaterial);
Vue.use(MdField);
Vue.use(MdProgress);
Vue.use(MdDialog);
Vue.use(MdButton);
Vue.use(MdChips);
Vue.use(VueRouter);
Vue.use(Datetime);
Vue.use(FullCalendar);
Vue.component('full-calendar', FullCalendar);
Vue.component('drag', Drag);
Vue.component('drop', Drop);

/**
 * Create vuex store
 */
let store = require('./vue/store/index');

/**
 * Prepare vue routing
 */
let router = new VueRouter({
  routes: routes,
  mode:   'history'
});

/**
 * Create a new Vue instance and attach it to the element with 'app' id
 */
const app = new Vue({
  el:     '#app',
  store:  store,
  router: router
});

/**
 * Set luxon locale
 */
 //Settings.defaultLocale = 'en';
